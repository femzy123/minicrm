defmodule MiniCrm.Repo.Migrations.CreateOpportunities do
  use Ecto.Migration

  def change do
    create table(:opportunities) do
      add :name, :string
      add :stage, :string
      add :close_date, :date
      add :probability, :decimal
      add :amount, :decimal
      add :discount, :decimal
      add :assigned_to, :string
      add :comment, :text
      add :account_id, references(:accounts, on_delete: :nothing)

      timestamps()
    end

    create index(:opportunities, [:account_id])
  end
end
