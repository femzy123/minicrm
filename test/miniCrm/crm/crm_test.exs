defmodule MiniCrm.CRMTest do
  use MiniCrm.DataCase

  alias MiniCrm.CRM


  describe "notes" do
    alias MiniCrm.Crm.Note

    @valid_attrs %{body: "some body"}
    @update_attrs %{body: "some updated body"}
    @invalid_attrs %{body: nil}

    def note_fixture(attrs \\ %{}) do
      {:ok, note} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Crm.create_note()

      note
    end

    test "list_notes/0 returns all notes" do
      note = note_fixture()
      assert Crm.list_notes() == [note]
    end

    test "get_note!/1 returns the note with given id" do
      note = note_fixture()
      assert Crm.get_note!(note.id) == note
    end

    test "create_note/1 with valid data creates a note" do
      assert {:ok, %Note{} = note} = Crm.create_note(@valid_attrs)
      assert note.body == "some body"
    end

    test "create_note/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Crm.create_note(@invalid_attrs)
    end

    test "update_note/2 with valid data updates the note" do
      note = note_fixture()
      assert {:ok, %Note{} = note} = Crm.update_note(note, @update_attrs)
      assert note.body == "some updated body"
    end

    test "update_note/2 with invalid data returns error changeset" do
      note = note_fixture()
      assert {:error, %Ecto.Changeset{}} = Crm.update_note(note, @invalid_attrs)
      assert note == Crm.get_note!(note.id)
    end

    test "delete_note/1 deletes the note" do
      note = note_fixture()
      assert {:ok, %Note{}} = Crm.delete_note(note)
      assert_raise Ecto.NoResultsError, fn -> Crm.get_note!(note.id) end
    end

    test "change_note/1 returns a note changeset" do
      note = note_fixture()
      assert %Ecto.Changeset{} = Crm.change_note(note)
    end
  end
end
