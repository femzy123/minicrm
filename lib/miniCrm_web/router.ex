defmodule MiniCrmWeb.Router do
  use MiniCrmWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MiniCrmWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/accounts", AccountController do
      resources "/notes", NoteController, only: [:create]
    end
    resources "/opportunities", OpportunityController
    resources "/leads", LeadController
    resources "/contacts", ContactController do
      post "/notes", NoteController, :create_contact
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", MiniCrmWeb do
  #   pipe_through :api
  # end
end
