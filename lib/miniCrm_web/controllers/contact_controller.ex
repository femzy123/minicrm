defmodule MiniCrmWeb.ContactController do
  use MiniCrmWeb, :controller

  alias MiniCrm.CRM
  alias MiniCrm.CRM.{Contact, Note}

  def index(conn, _params) do
    contacts = CRM.list_contacts()
    render(conn, "index.html", contacts: contacts)
  end

  def new(conn, _params) do
    changeset = CRM.change_contact(%Contact{})
    accounts = CRM.list_repo_accounts()
    countries = CRM.get_countries()
    title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
    render(conn, "new.html", changeset: changeset, accounts: accounts, countries: countries, title: title)
  end

  def create(conn, %{"contact" => contact_params}) do
    id = Map.get(contact_params, "account_id")
    account = CRM.get_account!(id)
    case CRM.create_contact(account, contact_params) do
      {:ok, contact} ->
        conn
        |> put_flash(:info, "Contact created successfully.")
        |> redirect(to: Routes.contact_path(conn, :show, contact))

      {:error, %Ecto.Changeset{} = changeset} ->
        accounts = CRM.list_repo_accounts()
        countries = CRM.get_countries()
        title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
        render(conn, "new.html", changeset: changeset, accounts: accounts, countries: countries, title: title)
    end
  end

  def show(conn, %{"id" => id}) do
    contact = CRM.get_contact!(id)
    note_changeset = CRM.change_note(%Note{})
    render(conn, "show.html", contact: contact, note_changeset: note_changeset)
  end

  def edit(conn, %{"id" => id}) do
    contact = CRM.get_contact!(id)
    changeset = CRM.change_contact(contact)
    render(conn, "edit.html", contact: contact, changeset: changeset)
  end

  def update(conn, %{"id" => id, "contact" => contact_params}) do
    contact = CRM.get_contact!(id)

    case CRM.update_contact(contact, contact_params) do
      {:ok, contact} ->
        conn
        |> put_flash(:info, "Contact updated successfully.")
        |> redirect(to: Routes.contact_path(conn, :show, contact))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contact: contact, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    contact = CRM.get_contact!(id)
    {:ok, _contact} = CRM.delete_contact(contact)

    conn
    |> put_flash(:info, "Contact deleted successfully.")
    |> redirect(to: Routes.contact_path(conn, :index))
  end
end
