defmodule MiniCrmWeb.LeadController do
  use MiniCrmWeb, :controller

  alias MiniCrm.CRM
  alias MiniCrm.CRM.{Lead, Contact}


  def index(conn, _params) do
    leads = CRM.list_leads()
    render(conn, "index.html", leads: leads)
  end

  def new(conn, _params) do
    changeset = CRM.change_lead(%Lead{})
    countries = CRM.get_countries()
    title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
    render(conn, "new.html", changeset: changeset, countries: countries, title: title)
  end

  def create(conn, %{"lead" => lead_params}) do
    case CRM.create_lead(lead_params) do
      {:ok, lead} ->
        conn
        |> put_flash(:info, "Lead created successfully.")
        |> redirect(to: Routes.lead_path(conn, :show, lead))

      {:error, %Ecto.Changeset{} = changeset} ->
        countries = CRM.get_countries()
        title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
        render(conn, "new.html", changeset: changeset, countries: countries, title: title)
    end
  end

  def show(conn, %{"id" => id}) do
    lead = CRM.get_lead!(id)
    accounts = CRM.list_repo_accounts()
    changeset = CRM.change_lead(lead)
    render(conn, "show.html", lead: lead, accounts: accounts, changeset: changeset)
  end

  def edit(conn, %{"id" => id}) do
    lead = CRM.get_lead!(id)
    countries = CRM.get_countries()
    title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
    changeset = CRM.change_lead(lead)
    render(conn, "edit.html", lead: lead, changeset: changeset, countries: countries, title: title)
  end


  # def update(conn, %{"id" => id, "lead" => lead_params}) do
  #   lead = CRM.get_lead!(id)

  #   case CRM.update_lead(lead, lead_params) do
  #     {:ok, lead} ->
  #       conn
  #       |> put_flash(:info, "Lead updated successfully.")
  #       |> redirect(to: Routes.lead_path(conn, :show, lead))

  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "edit.html", lead: lead, changeset: changeset)
  #   end
  # end

  def update(conn, %{"id" => id, "lead" => lead_params}) do
    lead = CRM.get_lead!(id)
    contact_params = convert_lead_to_contact(lead, lead_params["account_id"])
    account = CRM.get_contact_account(contact_params)
    status = %{"status" => "Converted"}

    case create_contact(account, contact_params) do
      {:ok} ->
        case CRM.update_lead(lead, status) do
          {:ok, lead} ->
            conn
            |> put_flash(:info, "Lead successfully linked to account.")
            |> redirect(to: Routes.lead_path(conn, :index))

          {:error, %Ecto.Changeset{} = changeset} ->
            accounts = CRM.list_accounts |> Enum.map(&{&1.name, &1.id})
            conn
            |> put_flash(:info, "Error linking lead to account")
            |> render("show.html", lead: lead, accounts: accounts, changeset: %Ecto.Changeset{})
            # render(conn, "show.html", lead: lead, accounts: accounts, changeset: changeset)
        end
      {:error} ->
        accounts = CRM.list_accounts |> Enum.map(&{&1.name, &1.id})
        conn
        |> put_flash(:info, "Error linking lead to account")
        |> render("show.html", lead: lead, accounts: accounts, changeset: %Ecto.Changeset{})
    end
  end

  def delete(conn, %{"id" => id}) do
    lead = CRM.get_lead!(id)
    {:ok, _lead} = CRM.delete_lead(lead)

    conn
    |> put_flash(:info, "Lead deleted successfully.")
    |> redirect(to: Routes.lead_path(conn, :index))
  end

  defp create_contact(account, contact_params) do
    case CRM.create_contact(account, contact_params) do
      {:ok, contact} -> {:ok}
      {:error, %Ecto.Changeset{} = changeset} -> {:error}
    end
  end

  defp convert_lead_to_contact(lead, account_id) do
    contact_params = %{"account_id" => account_id, "city" => lead.city, "country" => lead.country, "department" => lead.department, "email" => lead.email, "name" => lead.name, "phone" => lead.phone, "street" => lead.street, "title" => lead.title}
  end
end
