defmodule MiniCrmWeb.NoteController do
  use MiniCrmWeb, :controller

  alias MiniCrm.CRM


  def create(conn, %{"account_id" => account_id, "note" => note_params}) do
    account = CRM.get_account!(account_id)

    case CRM.create_note(account, note_params) do
      {:ok, _note} ->
        conn
        |> put_flash(:info, "Note created successfully.")
        |> redirect(to: Routes.account_path(conn, :show, account))
      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Issue creating note.")
        |> redirect(to: Routes.account_path(conn, :show, account))

      # {:error, %Ecto.Changeset{} = changeset} ->
      #   render(conn, "new.html", changeset: changeset)
    end
  end

  def create_contact(conn, %{"contact_id" => contact_id, "note" => note_params}) do
    contact = CRM.get_contact!(contact_id)

    case CRM.create_note_contact(contact, note_params) do
      {:ok, _note} ->
        conn
        |> put_flash(:info, "Note created successfully.")
        |> redirect(to: Routes.contact_path(conn, :show, contact))
      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Issue creating note.")
        |> redirect(to: Routes.contact_path(conn, :show, contact))

      # {:error, %Ecto.Changeset{} = changeset} ->
      #   render(conn, "new.html", changeset: changeset)
    end
  end

end
