defmodule MiniCrm.Repo do
  use Ecto.Repo,
    otp_app: :miniCrm,
    adapter: Ecto.Adapters.Postgres
end
