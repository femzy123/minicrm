defmodule MiniCrm.CRM.Note do
  use Ecto.Schema
  import Ecto.Changeset

  alias MiniCrm.CRM.{Account, Lead, Opportunity, Contact}

  schema "notes" do
    field :body, :string

    belongs_to :account, Account
    belongs_to :contact, Contact
    belongs_to :lead, Lead
    belongs_to :opportunity, Opportunity


    timestamps()
  end

  @doc false
  def changeset(note, attrs) do
    note
    |> cast(attrs, [:body])
    |> validate_required([:body])
  end
end
