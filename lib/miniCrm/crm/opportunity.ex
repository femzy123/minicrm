defmodule MiniCrm.CRM.Opportunity do
  use Ecto.Schema
  import Ecto.Changeset

  alias MiniCrm.CRM.Note

  schema "opportunities" do
    field :amount, :decimal
    field :assigned_to, :string
    field :close_date, :date
    field :comment, :string
    field :discount, :decimal
    field :name, :string
    field :probability, :decimal
    field :stage, :string
    belongs_to :account, MiniCrm.CRM.Account
    has_many :notes, Note



    timestamps()
  end

  @doc false
  def changeset(opportunity, attrs) do
    opportunity
    |> cast(attrs, [:name, :stage, :close_date, :probability, :amount, :discount, :assigned_to, :comment, :account_id])
    |> validate_required([:name, :stage, :close_date, :probability, :amount, :discount, :assigned_to, :comment, :account_id])
  end
end
